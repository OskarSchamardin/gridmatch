# README

A simple game, where the goal is to make lines with blocks to score points.

## Live demo

[Link to live demo](https://oskarschamardin.gitlab.io/gridmatch)


/* This file handles the configuration of a service worker that
 * will manage the working of the application.
 */

let staticCacheName = "v1";

self.addEventListener("install", (e) => {
    e.waitUntil(
        caches.open(staticCacheName).then((cache) => {
            return cache.addAll([
                "/gridmatch/",
                "/gridmatch/index.html",
                "/gridmatch/css/style.css",
                "/gridmatch/js/main.js",
            ]);
        })
    );
});

/* Network first strategy.
 * This attempts to fetch up to date data from the internet. It will show the
 * cached page only if the network fails. This will be a blocking event and
 * the user will have to wait a while if their connection is problematic.
 *
 * TODO: use the 'cache then network' strategy: https://developers.google.com/web/ilt/pwa/caching-files-with-service-worker#cache_then_network
 *
 * Note to anyone criticizing: I was lazy, its a monday night and i'm doing
 * this for fun. This is not a critical program (I hope). I will fix this in a
 * future commit, but not tonight. Thanks for your observarions into my
 * code. :)
 */
self.addEventListener('fetch', (event) => {
    event.respondWith(
        fetch(event.request).catch(() => {
            return caches.match(event.request);
        })
    );
});

const gridEdge = 9;         // Grid edge size in cells. Cell count is the square of the value.

document.addEventListener('DOMContentLoaded', () => {

    createGrid(gridEdge);
    createNewShape('shape-container0');
    createNewShape('shape-container1');
    createNewShape('shape-container2');
});

function createGrid(gridEdge) {
    /* Creates grid with cells that draggable elements can be dropped on */

    document.getElementById('grid').style.gridTemplateColumns = `repeat(${gridEdge}, 1fr)`;

    for(let i = 0; i < gridEdge ** 2; i++) {

        let newCell = document.createElement('div');
        newCell.id = `c${i}`;
        newCell.className = `grid-cell`;
        document.getElementById('grid').appendChild(newCell);
    }
}

function makeDraggable(el) {
    /* makes an element be draggable by mouse or touch */

    el.onmousedown = dragStartMouse;    // run function when dragging starts (mouse)
    el.onmouseup = dragStop;            // run function when dragging stops  (mouse)

    el.ontouchmove = dragStart;         // run function when dragging starts (touch)
    el.ontouchend = dragStop;           // run function when dragging stops  (touch)

    const elementStartX = el.offsetLeft;
    const elementStartY = el.offsetTop;

    function dragStartMouse(e) {
        /* start dragging element (mouse controls) */

        e = e || window.event;
        e.preventDefault();
        setDragStyle();

        /* get the mouse cursor position at startup */
        let pos3 = e.clientX;
        let pos4 = e.clientY;

        document.onmouseup = dragStop;
        document.onmousemove = function(e) {
            e = e || window.event;
            e.preventDefault();

            /* calculate the new cursor position */
            let pos1 = pos3 - e.clientX;
            let pos2 = pos4 - e.clientY;
            pos3 = e.clientX;
            pos4 = e.clientY;

            /* set the element's new position */
            el.style.top = (el.offsetTop - pos2) + "px";
            el.style.left = (el.offsetLeft - pos1) + "px";
        };
    }

    function dragStart(e) {
        /* start dragging element (touch controls) */

        e.preventDefault();
        setDragStyle();

        /* grab the location of the touch */
        let touchLocation = e.targetTouches[0];

        /* assign element new coordinates based on the touch (also make dragging look like dragging from the center) */
        el.style.left = touchLocation.pageX - el.offsetWidth / 2 + 'px';
        el.style.top = touchLocation.pageY - el.offsetHeight / 2 + 'px';
    }

    function dragStop(e) {
        /* instructions to run when dragging stops */

        unsetDragStyle();

        /* stop element dragging when mouse is released */
        document.onmouseup = null;
        document.onmousemove = null;

        /* get position when dropped */
        const elementStopX = parseInt(el.style.left);
        const elementStopY = parseInt(el.style.top);

        handleDrop(el, elementStartX, elementStartY, elementStopX, elementStopY);
    }

    function setDragStyle() {
        /* Add class that defines how element looks like during dragging */

        el.classList.add(`shape-drag`);
        Array.from(el.children).forEach((child, i) => {
            child.classList.add(`shape-cell-drag`);
        });
    }

    function unsetDragStyle() {
        /* Remove class that defines how element looks like during dragging */

        el.classList.remove(`shape-drag`);
        Array.from(el.children).forEach((child, i) => {
            child.classList.remove(`shape-cell-drag`);
        });
    }
}

function createNewShape(parentId) {
    /* Creates an element and makes it draggable */

    const newShape = document.createElement('div');
    newShape.classList.add(`shape`);
    document.getElementById(parentId).appendChild(newShape);

    /* TODO: random shapes by randomised selection from predefined list of
     * aprooved shape designs (give the 'for' loop below a meaningful upper number)
     */
    for(let i = 0; i < 25; i++) {
        const newCell = document.createElement('div');
        newCell.classList.add(`shape-cell`);
        newCell.style.gridArea = `c${i}`;
        newShape.appendChild(newCell);
    }

    makeDraggable(newShape);
}

function handleDrop(element, startX, startY, destX, destY) {
    /* Gets coordinates of grid squares and handles dragged element dropping */

    const closestCell = document.getElementById(getClosestCell(destX, destY));

    /* Determine distance between drop location and closest cell */
    const diffX = Math.abs(closestCell.offsetLeft - destX);
    const diffY = Math.abs(closestCell.offsetTop - destY);
    const proximityThreshold = closestCell.offsetWidth / 1.5;

    /* If drop location is within half of the cell's width (some overlap) */
    if(diffX <= proximityThreshold && diffY <= proximityThreshold) {
        /* Count as placed */

        /* TODO: get closest cell to first shape's cell */
        /* TODO: check if shape's cells are trying to occupy already occupied cells */
        /* Check if cell is marked as occupied */
        if(checkOccupied(closestCell)) {
            /* Occupied */

            /* Move element to reserve */
            moveToPosition(element, startX, startY);
        } else {
            /* Not occupied */

            let row = 0;
            let emptyCellsNo = 0;
            /* Loop shape's size */
            /* TODO: Make loop compatible with different size shapes */
            for(let i = 0; i < 25; i++) {
                if(i !== 0 && i % 5 === 0) { row++; }
                let rowModifier = 0;
                if(row > 0) { rowModifier = 4 * row }

                try {
                    /* If cell does not match with current cell count */
                    if(window.getComputedStyle(element.children[i - emptyCellsNo], null).getPropertyValue("grid-row-start").substring(1) !== String(i)) {
                        emptyCellsNo++;
                        continue;
                    }
                } catch(e) {
                    if(e instanceof TypeError) {
                        continue;
                    }
                }
                markOccupied(document.getElementById(`c${Number(closestCell.id.substring(1)) + i + rowModifier}`));
                setScore(getScore() + 10);
            }

            element.remove();           /* Delete element, this makes it look like it's been placed into the board */
            if(isReserveEmpty()) {
                /* Empty */
                createNewShape('shape-container0');
                createNewShape('shape-container1');
                createNewShape('shape-container2');
            }

            checkCompletedLines();
        }
    } else {
        /* Count as missed */

        /* Move element to reserve */
        moveToPosition(element, startX, startY);
    }

    function isReserveEmpty() {
        /* Checks if reserve area is empty. True if empty, false if at least one child */

        let noChildren = true

        /* Loop shape containers */
        Array.from(document.getElementById('reserve').children).forEach((child, i) => {
            if (child.hasChildNodes()) {
                noChildren = false;
            }
        });

        return noChildren;
    }
}

function checkCompletedLines() {
    /* Checks if any lines are fully occupied. Handles score and unoccupying the cells */

    let cellsToClear = [];

    checkLines(false);  // Horizontal
    checkLines(true);   // Vertical

    function checkLines(checkVertical) {
        /* Checks if Horizontal/Vertical lines are occupied this is a function to reduce code repetition */

        const horizMod = checkVertical ? 1 : gridEdge;
        const vertMod = checkVertical ? gridEdge : 1;

        for(let i = 0; i < gridEdge; i++) {
            /* Loop first cell in every line (horizontal) */

            if (!checkOccupied(document.getElementById(`c${i * horizMod}`))) {
                continue;   // Skip line checking if first cell is unoccupied
            }

            let occupiedCellsInLine = [];
            for(let j = 0; j < gridEdge; j++) {
                /* Loop line, starting from second cell (horizontal) */
                if (!checkOccupied(document.getElementById(`c${i * horizMod + j * vertMod}`))) {
                    break;  // Stop checking if unoccupied cell is present
                } else {
                    occupiedCellsInLine.push(`c${i * horizMod + j * vertMod}`);
                }

                if(occupiedCellsInLine.length == gridEdge) {
                    cellsToClear.push.apply(cellsToClear, occupiedCellsInLine);
                    setScore(getScore() + 1000);
                }
            }
        }
    }

    /* Unoccupy cell */
    cellsToClear.forEach((cell, i) => {
        document.getElementById(cell).classList.remove('occupied');
    });
}

function checkOccupied(cell) {
    /* Checks if cell's class contains "occupied". Returns true if occupied, false if not */

    return cell.classList.contains("occupied");
}

function markOccupied(cell) {
    /* Mark cell as occupied */

    cell.classList.add("occupied");          // Mark cell as occupied, things can no longer be dropped on it
}

function moveToPosition(element, x, y) {
    /* Sets element positon to given coords */

    element.style.left = `${x}px`;
    element.style.top = `${y}px`;
}

function setScore(newScore) {
    /* Sets the score counter to given number. Prepends 0s if score is low enough */

    const scoreLength = newScore.toString().length;

    if(newScore.toString().length <= 9) {
        for(let i = 0; i < 9 - scoreLength; i++) {
            newScore = `0${newScore}`;
        }
    }

    document.getElementById('score').innerText = newScore;
}

function getScore() {
    /* Returns score as number */

    return Number(document.getElementById('score').innerText);
}

function getClosestCell(dropX, dropY) {
    /* Returns the id of the closest cell in grid. */

    let closestCell;

    /* Loop grid cells */
    Array.from(document.getElementById('grid').children).forEach((cell, i) => {
        let x = cell.offsetLeft;
        let y = cell.offsetTop;

        /* Set default value */
        if(closestCell === undefined) {
            closestCell = {x: x, y: y, id: cell.id};
        }

        /* Set new id if a closer cell is found */
        if(Math.abs(x - dropX) <= Math.abs(dropX - closestCell.x) && Math.abs(dropY - y) <= Math.abs(dropY - closestCell.y)) {
            closestCell = {x: x, y: y, id: cell.id};
        }
    });

    return closestCell.id;
}
